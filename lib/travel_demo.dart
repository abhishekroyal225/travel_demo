import 'dart:async';

import 'package:flutter/material.dart';
import './all_colors.dart' as all_Colors;

class TravelDemo extends StatefulWidget {
  @override
  _TravelDemoState createState() => _TravelDemoState();
}

class _TravelDemoState extends State<TravelDemo> {
  int _index = 0;
  double _height, _width;
  PageController _pageController = PageController(
    initialPage: 0,
    viewportFraction: 0.8,
  );
  List<int> _indexList = [1, 2, 3];
  StreamController<int> _indicatorController = StreamController<int>();
  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: all_Colors.bodyBackgroundColor,
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/icons/firsticon.png')),
            title: Text('Wallet'),
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/icons/secondicon.png')),
            title: Text('Team'),
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/icons/thirdicon.png')),
            title: Text('Requests'),
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/icons/forthicon.png')),
            title: Text('More'),
          ),
        ],
        // elevation: 3.0,
        iconSize: 25.0,
        currentIndex: _index,
        selectedItemColor: all_Colors.bottomiconcolor,
        showUnselectedLabels: true,
        unselectedItemColor: all_Colors.bottomunselectedIconColor,
        onTap: (int position) => setState(() => _index = position),
      ),
      body: Container(
        margin: EdgeInsets.only(
          top: _height * 0.14,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                left: _width * 0.06,
              ),
              child: Text(
                'Wallets',
                style: TextStyle(
                    fontSize: 32,
                    fontFamily: 'muli',
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: _height * 0.05,
            ),
            Container(
              height: _height * 0.23,
              child: PageView(
                controller: _pageController,
                onPageChanged: (value) => _indicatorController.add(value),
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      left: _width * 0.001,
                      right: _width * 0.02,
                    ),
                    decoration: BoxDecoration(
                        color: all_Colors.firstCardColor,
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.bottomRight,
                          child:
                              Image.asset('assets/icons/downwardecliipes.png'),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Image.asset('assets/icons/upwardecliipse.png'),
                        ),
                        _walletCardItems(),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: _width * 0.04,
                    ),
                    decoration: BoxDecoration(
                        color: all_Colors.secondCardColor,
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.bottomRight,
                          child:
                              Image.asset('assets/icons/downwardecliipes.png'),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Image.asset('assets/icons/upwardecliipse.png'),
                        ),
                        _walletCardItems(),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: _width * 0.04,
                      right: _width * 0.01,
                    ),
                    decoration: BoxDecoration(
                        color: all_Colors.fourthCardColor,
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.bottomRight,
                          child:
                              Image.asset('assets/icons/downwardecliipes.png'),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Image.asset('assets/icons/upwardecliipse.png'),
                        ),
                        _walletCardItems(),
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: _height * 0.02),
            // Sliding Controller
            StreamBuilder(
                stream: _indicatorController.stream,
                initialData: 0,
                builder: (context, snapshot) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: _indexList.map((url) {
                      int index = _indexList.indexOf(url);
                      return (index == snapshot.data)
                          ? Container(
                              height: 8,
                              width: 13,
                              margin: EdgeInsets.only(right: 3),
                              decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                            )
                          : Container(
                              height: 8,
                              width: 8,
                              margin: EdgeInsets.only(right: 5),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.grey,
                              ),
                            );
                    }).toList(),
                  );
                }),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: _height * 0.034),
                padding: EdgeInsets.only(
                  left: _width * 0.06,
                  right: _width * 0.07,
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(50.0),
                    topRight: Radius.circular(50.0),
                  ),
                ),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        top: _height * 0.02,
                        bottom: _height * 0.02,
                      ),
                      child: Image.asset('assets/icons/rectangle.png'),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: _width * 0.01),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Transactions',
                            style: TextStyle(
                                fontSize: 30,
                                fontFamily: 'muli',
                                fontWeight: FontWeight.bold),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                margin: EdgeInsets.only(right: _width * 0.01),
                                child: Image.asset(
                                  'assets/icons/fifth.png',
                                  alignment: Alignment.bottomRight,
                                  scale: 0.8,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: _height * 0.014),
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: _width * 0.02),
                            child: Text(
                              'Today',
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 24,
                                fontFamily: 'muli',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                margin: EdgeInsets.only(right: _width * 0.02),
                                child: RichText(
                                  text: TextSpan(
                                    text: '-260',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 22,
                                    ),
                                    children: [
                                      TextSpan(
                                          text: '.95',
                                          style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 22,
                                          ))
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 0,
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                child: Text(
                                  'S\$',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: _height * 0.07,
                      margin: EdgeInsets.only(top: _height * 0.014),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: _width * 0.18,
                            margin: EdgeInsets.only(left: _width * 0.02),
                            decoration: BoxDecoration(
                                color: Colors.grey[100],
                                borderRadius:
                                    BorderRadius.all(Radius.circular(16.0)),
                                image: DecorationImage(
                                  image: AssetImage('assets/icons/grab.png'),
                                  scale: 0.7,
                                )),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: _width * 0.02,
                              top: _height * 0.012,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Grab Taxi',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'muli',
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: _height * 0.003),
                                  child: Text(
                                    'Sales Team',
                                    style: TextStyle(
                                      color: Colors.grey[600],
                                      fontSize: 16,
                                      fontFamily: 'muli',
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                margin: EdgeInsets.only(right: _width * 0.02),
                                child: RichText(
                                  text: TextSpan(
                                    text: '10',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 22,
                                    ),
                                    children: [
                                      TextSpan(
                                          text: '.90',
                                          style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 22,
                                          ))
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 0,
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                child: Text(
                                  'S\$',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: _height * 0.07,
                      margin: EdgeInsets.only(top: _height * 0.014),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: _width * 0.18,
                            margin: EdgeInsets.only(left: _width * 0.02),
                            decoration: BoxDecoration(
                                color: Colors.grey[100],
                                borderRadius:
                                    BorderRadius.all(Radius.circular(18.0)),
                                image: DecorationImage(
                                  image:
                                      AssetImage('assets/icons/airlines.png'),
                                  scale: 0.7,
                                )),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: _width * 0.02,
                              top: _height * 0.012,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Singapore Airlines',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'muli',
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: _height * 0.003),
                                  child: Text(
                                    'Business Travel',
                                    style: TextStyle(
                                      color: Colors.grey[600],
                                      fontSize: 16,
                                      fontFamily: 'muli',
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                margin: EdgeInsets.only(right: _width * 0.02),
                                child: RichText(
                                  text: TextSpan(
                                    text: '250',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 22,
                                    ),
                                    children: [
                                      TextSpan(
                                          text: '.09',
                                          style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 22,
                                          ))
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 0,
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                child: Text(
                                  'S\$',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: _height * 0.014),
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: _width * 0.01),
                            child: Text(
                              '9, March',
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 24,
                                fontFamily: 'muli',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                margin: EdgeInsets.only(right: _width * 0.02),
                                child: RichText(
                                  text: TextSpan(
                                    text: '1,269',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 22,
                                    ),
                                    children: [
                                      TextSpan(
                                          text: '.95',
                                          style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 22,
                                          ))
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 0,
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                child: Text(
                                  'S\$',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _walletCardItems() => Container(
        margin: EdgeInsets.only(
          top: _height * 0.02,
          left: _width * 0.02,
        ),
        padding: EdgeInsets.only(right:10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Sales Team',
                    style: TextStyle(
                      color: all_Colors.bodyBackgroundColor,
                      fontSize: 18,
                    ),
                  ),
                  Container(
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                      color: Colors.black.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Center(
                      child: Image.asset(
                        'assets/icons/Icon.png',
                        alignment: Alignment.bottomRight,
                        scale: 0.8,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Text(
                'S\$2671.4',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 28,
                  fontFamily: 'muli',
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: _height * 0.026),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Image.asset('assets/icons/star.png'),
                  ),
                  Container(
                    child: Image.asset('assets/icons/star.png'),
                  ),
                  Container(
                    child: Image.asset('assets/icons/star.png'),
                  ),
                  Container(
                    child: Text(
                      '4483',
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'ocra',
                        fontSize: 20,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: _height * 0.01),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'CARD TYPE',
                    style: TextStyle(
                        color: all_Colors.bodyBackgroundColor,
                        fontSize: 12,
                        fontFamily: 'muli'),
                  ),
                  Container(
                    child: Text(
                      'VALID THRU',
                      style: TextStyle(
                        color: all_Colors.bodyBackgroundColor,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: _height * 0.001),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'VIRTUAL',
                    style: TextStyle(
                      color: all_Colors.bodyBackgroundColor,
                      fontSize: 20,
                      fontFamily: 'ocra',
                    ),
                  ),
                  Container(
                    child: Text(
                      '12/22',
                      style: TextStyle(
                        color: all_Colors.bodyBackgroundColor,
                        fontSize: 20,
                        fontFamily: 'ocra',
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}
